/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parqueaderoweb.controlador;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.Date;
import javax.annotation.PostConstruct;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.DiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.FlowChartConnector;
import org.primefaces.model.diagram.endpoint.BlankEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;
import org.primefaces.model.diagram.overlay.ArrowOverlay;
import org.primefaces.model.diagram.overlay.LabelOverlay;
import parqueaderolse.controlador.ListaSE;
import parqueaderolse.controlador.excepciones.ParqueaderoExcepcion;
import parqueaderolse.modelo.Automovil;
import parqueaderolse.modelo.Buseta;
import parqueaderolse.modelo.Moto;
import parqueaderolse.modelo.Nodo;
import parqueaderolse.modelo.Vehiculo;
import parqueaderoweb.utilidades.JsfUtil;

/**
 *
 * @author carloaiza
 */
@Named(value = "beanParqueadero")
@SessionScoped
public class BeanParqueadero implements Serializable {

    private boolean deshabilitarNuevo = true;
    private int tipoVehiculoSeleccionado;

    private Nodo nodoMostrar = new Nodo(new Vehiculo());
    private boolean opcionSeleccionado;
    private byte numeroAsientos = 5;

    private ListaSE listaVehiculos = new ListaSE();

    private DefaultDiagramModel model;

    /**
     * Creates a new instance of BeanParqueadero
     */
    public BeanParqueadero() {
    }

    public DefaultDiagramModel getModel() {
        return model;
    }

    public void setModel(DefaultDiagramModel model) {
        this.model = model;
    }

    public boolean isOpcionSeleccionado() {
        return opcionSeleccionado;
    }

    public void setOpcionSeleccionado(boolean opcionSeleccionado) {
        this.opcionSeleccionado = opcionSeleccionado;
    }

    public byte getNumeroAsientos() {
        return numeroAsientos;
    }

    public void setNumeroAsientos(byte numeroAsientos) {
        this.numeroAsientos = numeroAsientos;
    }

    public ListaSE getListaVehiculos() {
        return listaVehiculos;
    }

    public void setListaVehiculos(ListaSE listaVehiculos) {
        this.listaVehiculos = listaVehiculos;
    }

    public Nodo getNodoMostrar() {
        return nodoMostrar;
    }

    public void setNodoMostrar(Nodo nodoMostrar) {
        this.nodoMostrar = nodoMostrar;
    }

    public int getTipoVehiculoSeleccionado() {
        return tipoVehiculoSeleccionado;
    }

    public void setTipoVehiculoSeleccionado(int tipoVehiculoSeleccionado) {
        this.tipoVehiculoSeleccionado = tipoVehiculoSeleccionado;
    }

    public boolean isDeshabilitarNuevo() {
        return deshabilitarNuevo;
    }

    public void setDeshabilitarNuevo(boolean deshabilitarNuevo) {
        this.deshabilitarNuevo = deshabilitarNuevo;
    }

    public void habilitarGuardado() {
        deshabilitarNuevo = false;
        nodoMostrar = new Nodo(new Vehiculo());
        tipoVehiculoSeleccionado = 0;
        opcionSeleccionado = true;
        numeroAsientos = 5;
    }

    private void seleccionarTipoVehiculo() {
        if (nodoMostrar.getDato() instanceof Moto) {
            tipoVehiculoSeleccionado = 1;
            Moto moto = (Moto) nodoMostrar.getDato();
            opcionSeleccionado = moto.isCasco();

        } else if (nodoMostrar.getDato() instanceof Automovil) {
            tipoVehiculoSeleccionado = 2;
            Automovil auto = (Automovil) nodoMostrar.getDato();
            opcionSeleccionado = auto.isPasacintas();
        } else if (nodoMostrar.getDato() instanceof Buseta) {
            tipoVehiculoSeleccionado = 3;
            Buseta buseta = (Buseta) nodoMostrar.getDato();
            numeroAsientos = buseta.getNumeroAsientos();
        }
    }

    public void irAlPrimero() {
        nodoMostrar = listaVehiculos.getCabeza();
        seleccionarTipoVehiculo();
    }

    public void irAlSiguiente() {
        if (nodoMostrar.getSiguiente() != null) {
            nodoMostrar = nodoMostrar.getSiguiente();
            seleccionarTipoVehiculo();
        }
    }

    public void irAlUltimo() {

    }

    @PostConstruct
    public void llenarVehiculos() {
        try {
            listaVehiculos.adicionarNodo(new Automovil(true, "NAE033", new Date()));
            listaVehiculos.adicionarNodo(new Moto(false, "TVL03D", new Date()));
            listaVehiculos.adicionarNodo(new Buseta((byte) 30, "NAC995", new Date()));
            irAlPrimero();
        } catch (ParqueaderoExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }
        inicializarGrafico();

    }

    public void guardarVehiculo() {
        Vehiculo vehiculo = null;
        switch (tipoVehiculoSeleccionado) {
            case 1:
                //Moto
                vehiculo = new Moto(opcionSeleccionado, nodoMostrar.getDato().getPlaca(),
                        nodoMostrar.getDato().getFechaHoraEntrada());
                break;
            case 2:
                //Automovil
                vehiculo = new Automovil(opcionSeleccionado, nodoMostrar.getDato().getPlaca(),
                        nodoMostrar.getDato().getFechaHoraEntrada());
                break;
            case 3:
                //Buseta
                vehiculo = new Buseta(numeroAsientos, nodoMostrar.getDato().getPlaca(),
                        nodoMostrar.getDato().getFechaHoraEntrada());
                break;

        }
        try {
            listaVehiculos.adicionarNodo(vehiculo);
            deshabilitarNuevo = true;
            tipoVehiculoSeleccionado = 0;
            irAlPrimero();
            JsfUtil.addSuccessMessage("Se ha adicionado con éxito");
        } catch (ParqueaderoExcepcion ex) {
            JsfUtil.addErrorMessage(ex.getMessage());
        }

    }

    public void guardarVehiculoAlInicio() {

        listaVehiculos.adicionarNodoAlInicio(nodoMostrar.getDato());
        deshabilitarNuevo = true;
        tipoVehiculoSeleccionado = 0;
        irAlPrimero();

    }

    public void invertir() {
        listaVehiculos.invertirLista();
        irAlPrimero();
    }

    public void eliminarVehiculo() {

        listaVehiculos.eliminarNodo(nodoMostrar.getDato());
        irAlPrimero();

    }

    public void inicializarGrafico() {
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        //Defino el conector por defecto
        FlowChartConnector connector = new FlowChartConnector();
        connector.setPaintStyle("{strokeStyle:'#C7B097',lineWidth:3}");
        model.setDefaultConnector(connector);

        if (getListaVehiculos().getCabeza() != null) {
            Nodo temp = getListaVehiculos().getCabeza();
            int x=10;
            int y=4;
            while(temp != null)
            {
                 Element elemento = new Element(temp.getDato().getPlaca(), 
                         x+"em", y+"em");
                 model.addElement(elemento);
                elemento.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP));
                elemento.addEndPoint(new BlankEndPoint(EndPointAnchor.BOTTOM_RIGHT));
                temp= temp.getSiguiente();
                x+=15;
                y+=7;
            }
            
            for(int i=0; i < model.getElements().size() -1; i++)
            {
                model.connect(createConnection(model.getElements().get(i).getEndPoints().get(1), 
                model.getElements().get(i+1).getEndPoints().get(0), "Sig"));
            }
            
           

            
        }
        /*
        Element start = new Element("Fight for your dream", "20em", "6em");
        start.addEndPoint(new BlankEndPoint(EndPointAnchor.BOTTOM));
        start.addEndPoint(new BlankEndPoint(EndPointAnchor.LEFT));
         
        Element trouble = new Element("Do you meet some trouble?", "20em", "18em");
        trouble.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP));
        trouble.addEndPoint(new BlankEndPoint(EndPointAnchor.BOTTOM));
        trouble.addEndPoint(new BlankEndPoint(EndPointAnchor.RIGHT));
         
        Element giveup = new Element("Do you give up?", "20em", "30em");
        giveup.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP));
        giveup.addEndPoint(new BlankEndPoint(EndPointAnchor.LEFT));
        giveup.addEndPoint(new BlankEndPoint(EndPointAnchor.RIGHT));
         
        Element succeed = new Element("Succeed", "50em", "18em");
        succeed.addEndPoint(new BlankEndPoint(EndPointAnchor.LEFT));
        succeed.setStyleClass("ui-diagram-success");
         
        Element fail = new Element("Fail", "50em", "30em");
        fail.addEndPoint(new BlankEndPoint(EndPointAnchor.LEFT));
        fail.setStyleClass("ui-diagram-fail");
                         
        model.addElement(start);
        model.addElement(trouble);
        model.addElement(giveup);
        model.addElement(succeed);
        model.addElement(fail);
                 
        model.connect(createConnection(start.getEndPoints().get(0), trouble.getEndPoints().get(0), null));
        model.connect(createConnection(trouble.getEndPoints().get(1), giveup.getEndPoints().get(0), "Yes"));
        model.connect(createConnection(giveup.getEndPoints().get(1), start.getEndPoints().get(1), "No"));
        model.connect(createConnection(trouble.getEndPoints().get(2), succeed.getEndPoints().get(0), "No"));
        model.connect(createConnection(giveup.getEndPoints().get(2), fail.getEndPoints().get(0), "Yes"));
         */
    }

    ///Une dos puntos de finalizacion con el conector que se haya definido 
    private Connection createConnection(EndPoint from, EndPoint to, String label) {
        Connection conn = new Connection(from, to);
        conn.getOverlays().add(new ArrowOverlay(20, 20, 1, 1));

        if (label != null) {
            conn.getOverlays().add(new LabelOverlay(label, "flow-label", 0.5));
        }

        return conn;
    }

}
