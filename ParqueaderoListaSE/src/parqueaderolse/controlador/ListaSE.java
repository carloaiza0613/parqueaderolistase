/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parqueaderolse.controlador;

import java.util.ArrayList;
import java.util.List;
import parqueaderolse.controlador.excepciones.ParqueaderoExcepcion;
import parqueaderolse.modelo.Automovil;
import parqueaderolse.modelo.Buseta;
import parqueaderolse.modelo.Moto;
import parqueaderolse.modelo.Nodo;
import parqueaderolse.modelo.Vehiculo;

/**
 *
 * @author carloaiza
 */
public class ListaSE {

    public Nodo cabeza;

    public ListaSE() {
    }
    

    public Nodo getCabeza() {
        return cabeza;
    }

    public void setCabeza(Nodo cabeza) {
        this.cabeza = cabeza;
    }

    public int contarNodos() {
        
        if (cabeza != null) {
            int cont = 0;
            Nodo temp = cabeza;
            while (temp != null) {
                cont++;
                temp = temp.getSiguiente();

            }
            return cont;
        }
        return 0;
    }

    public String listarNodos() {
        if (cabeza != null) {
            String listado = "";
            Nodo temp = cabeza;
            while (temp != null) {
                listado += temp.getDato();
                temp = temp.getSiguiente();

            }
            return listado;
        }
        return "No hay datos";
    }
    
    public List<Vehiculo> listarNodosVehiculos() {
        if (cabeza != null) {
            List<Vehiculo> listado = new ArrayList<>();
            Nodo temp = cabeza;
            while (temp != null) {
                listado.add(temp.getDato());
                temp = temp.getSiguiente();

            }
            return listado;
        }
        return null;
    }
    
    
    public void adicionarNodo(Vehiculo dato) throws ParqueaderoExcepcion
    {
        
        if(cabeza!=null)
        {
            verificarExistenciaVehiculo(dato);
            Nodo temp= cabeza;
            while(temp.getSiguiente()!=null)
            {
                temp= temp.getSiguiente();
            }   
            temp.setSiguiente(new Nodo(dato));
        }   
        else
        {
           cabeza = new Nodo(dato);
        }    
    }
    
    
    public void adicionarNodoAlInicio(Vehiculo dato)
    {
        if(cabeza!=null)
        {
            Nodo nuevo= new Nodo(dato);
            nuevo.setSiguiente(cabeza);
            cabeza= nuevo;            
        }   
        else
        {
           cabeza = new Nodo(dato);
        }
    }

    
    public void invertirLista()
    {
        Nodo temp=cabeza;
        ListaSE listaCopia= new ListaSE();
        
        while(temp!=null)
        {
            listaCopia.adicionarNodoAlInicio(temp.getDato());            
            temp= temp.getSiguiente();
        }
        cabeza= listaCopia.getCabeza();
        
        
    }
    
    public boolean eliminarNodo(Vehiculo dato)
    {
        if(cabeza!=null)
        {
            if(cabeza.getDato().getPlaca().equals(dato.getPlaca()))
            {
                cabeza=cabeza.getSiguiente();
                return true;
            }
            else
            {
                Nodo temp=cabeza;
                while(temp != null)
                {
                    if(temp.getSiguiente()!=null)
                    {
                        if(temp.getSiguiente().getDato().getPlaca().equals(dato.getPlaca()))
                        {
                            temp.setSiguiente(temp.getSiguiente().getSiguiente());
                            return true;
                        }    
                    }   
                    temp=temp.getSiguiente();
                } 
            }    
        }        
        return false;
    }    


    public void verificarExistenciaVehiculo(Vehiculo veh) 
            throws ParqueaderoExcepcion
    {
        if(cabeza!=null)
        {
            if(veh.getPlaca().startsWith("1"))
            {
                throw new ParqueaderoExcepcion("Las placas no pueden iniciar con un número");
            }
            
            Nodo temp=cabeza;
            while(temp!=null)
            {
                if(temp.getDato().getPlaca().equals(veh.getPlaca()))
                {
                    throw new ParqueaderoExcepcion("El vehículo ya existe");
                }
                temp= temp.getSiguiente();
            }
            
        }
    }
    
}
