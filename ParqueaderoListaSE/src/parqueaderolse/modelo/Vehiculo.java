/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parqueaderolse.modelo;

import java.util.Date;

/**
 *dato
 * @author carloaiza
 */
public class Vehiculo {
    private String placa;
    private Date fechaHoraEntrada;
    private Date fechHoraSalida;
    private double valorAPagar;

    public Vehiculo() {
    }
    

    public Vehiculo(String placa, Date fechaHoraEntrada) {
        this.placa = placa;
        this.fechaHoraEntrada = fechaHoraEntrada;
    }

    
    
    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public Date getFechaHoraEntrada() {
        return fechaHoraEntrada;
    }

    public void setFechaHoraEntrada(Date fechaHoraEntrada) {
        this.fechaHoraEntrada = fechaHoraEntrada;
    }

    public Date getFechHoraSalida() {
        return fechHoraSalida;
    }

    public void setFechHoraSalida(Date fechHoraSalida) {
        this.fechHoraSalida = fechHoraSalida;
    }

    public double getValorAPagar() {
        return valorAPagar;
    }

    public void setValorAPagar(double valorAPagar) {
        this.valorAPagar = valorAPagar;
    }
    
    
    
}
