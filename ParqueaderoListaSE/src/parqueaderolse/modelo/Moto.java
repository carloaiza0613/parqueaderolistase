/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parqueaderolse.modelo;

import java.util.Date;

/**
 *
 * @author carloaiza
 */
public class Moto extends Vehiculo{
    private boolean casco;

    public Moto(boolean casco, String placa, Date fechaHoraEntrada) {
        super(placa, fechaHoraEntrada);
        this.casco = casco;
    }

    public boolean isCasco() {
        return casco;
    }

    public void setCasco(boolean casco) {
        this.casco = casco;
    }
    
    
    
}
