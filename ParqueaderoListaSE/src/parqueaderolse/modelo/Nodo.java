/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parqueaderolse.modelo;

import parqueaderolse.modelo.Vehiculo;

/**
 *
 * @author carloaiza
 */
public class Nodo {
    private Vehiculo dato;
    private Nodo siguiente;

    public Nodo(Vehiculo dato) {
        this.dato = dato;
    }
    
    

    public Vehiculo getDato() {
        return dato;
    }

    public void setDato(Vehiculo dato) {
        this.dato = dato;
    }

    public Nodo getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(Nodo siguiente) {
        this.siguiente = siguiente;
    }
    
    
    
}
