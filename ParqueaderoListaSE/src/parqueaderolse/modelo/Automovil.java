/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parqueaderolse.modelo;

import java.util.Date;

/**
 *
 * @author carloaiza
 */
public class Automovil extends Vehiculo {
    private boolean pasacintas;

    public Automovil(boolean pasacintas, String placa, Date fechaHoraEntrada) {
        super(placa, fechaHoraEntrada);
        this.pasacintas = pasacintas;
    }

    public boolean isPasacintas() {
        return pasacintas;
    }

    public void setPasacintas(boolean pasacintas) {
        this.pasacintas = pasacintas;
    }
    
    
}
